#!/usr/bin/env python3

import rospy
import actionlib

import math
from math import radians, degrees

from geometry_msgs.msg import Point
import tf
import traceback
import time
import sys

from nav_msgs.msg import Path
import roslib
import math
import os
from std_msgs.msg import Header
from std_msgs.msg import String
import tf.transformations
import tf2_ros
import tf2_geometry_msgs
from geometry_msgs.msg import PointStamped
from geometry_msgs.msg import Pose
from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import WrenchStamped
from geometry_msgs.msg import Vector3Stamped
from geometry_msgs.msg import Vector3
from sensor_msgs.msg import LaserScan
import cv2
import numpy as np
import rospy
from geometry_msgs.msg import Twist


rospy.loginfo("Start of simple follow me!")
BURGER_MAX_LIN_VEL = 0.22
BURGER_MAX_ANG_VEL = 2.84/3

WAFFLE_MAX_LIN_VEL = 0.26
WAFFLE_MAX_ANG_VEL = 1.82

LIN_VEL_STEP_SIZE = 0.01
ANG_VEL_STEP_SIZE = 0.1
e = "Communications Failed"
turtlebot3_model = rospy.get_param("model", "burger")

# Returns the smallest distance between 2 angles
def angle_diff(a, b):
    d = a - b
    d = ((d + math.pi) % (2 * math.pi)) - math.pi
    return d

class SimpleFollowMe:
    """Sends twist commands to the robot base based on LIDAR info
    """

    def __init__(self, hmi_wrapper=None):
        rospy.loginfo("Starting simple_follow_me...")

        self.pub = rospy.Publisher('cmd_vel', Twist, queue_size=10)
        self.tick_period = 0.01
        self.laser_scan = None
        rospy.Subscriber("/tb3/scan", LaserScan,  self.scan_callback.__func__ )


        status = 0
        self.target_linear_vel   = 0.0
        self.target_angular_vel  = 0.0
        
    def _shutdown(self):
        twist = Twist()
        twist.angular.x = 0.0
        twist.angular.y = 0.0
        twist.angular.z = 0.0

        self.pub.publish(twist)

    def scan_callback(self, msg) :
        self.laser_scan = msg

    def get_barycenter_offset(self, range_min, range_max, detection_angle, verbose=False):
        """Listen to the /scan LaserScan topic and outputs dlin_percent and dang_percent which represent how far away from the center of the chosen shape the barycenter of points is.
        """
        # frame_id: "base_scan"
        # angle_min: 0.0
        # angle_max: 6.2657318115234375
        # angle_increment: 0.01745329238474369
        # time_increment: 2.9880000511184335e-05
        # scan_time: 0.0
        # range_min: 0.11999999731779099
        # range_max: 3.5
        if self.laser_scan is None :
            return 0, 0
        absolute_range_max = 3.5
        absolute_angle_max = 6.2657318115234375
        half_angle = detection_angle/2
        # rospy.loginfo("Waiting for laser scan")
        # laser_scan = rospy.wait_for_message("/tb3/scan", LaserScan, timeout=None)
        # rospy.loginfo("laser scan received!")

        angle_increment = self.laser_scan.angle_increment
        pixel_per_meter = 250
        image_size = int(absolute_range_max * pixel_per_meter)
        height = image_size
        width = image_size
        rospy.loginfo("Image will be {}x{}".format(width, height))
        image = np.zeros((height, width, 3), np.uint8)
        index = -1
        center_x = width / 2
        center_y = 0.5*(range_max + range_min) * pixel_per_meter
        sum_x = 0
        sum_y = 0
        
        nb_points = 0
        if verbose :
            for i in range(width):
                for j in range(height) :
                    angle = math.atan2(i-(width/2), j)
                    dist = math.sqrt(((width/2)-i)**2 + j**2)/pixel_per_meter
                    if dist >= range_min and dist <= range_max and abs(angle_diff(angle, 0)) < half_angle:
                        image[j, i] = (50, 50, 100)  # y, x as always

        # the 0, 0 point of the laserscan will be drawn on the point x = width/2 and y = 0 (so the top of the image)
        for r in self.laser_scan.ranges:
            index += 1
            d = 0
            try:
                d = float(r)
                if np.isnan(d):
                    continue
            except:
                # inf?
                continue
            if d < range_min or d >= range_max:
                continue
            angle = self.laser_scan.angle_min + index * angle_increment
            if abs(angle_diff(angle, 0)) > half_angle:
                continue
            x = int(round(width / 2 + d * pixel_per_meter * math.sin(angle)))
            y = int(round(d * pixel_per_meter * math.cos(angle)))
            sum_x += x
            sum_y += y
            nb_points +=1
            if x >= 0 and x < width and y >= 0 and y < height:
                image[y, x] = (255, 255, 255)  # y, x as always
            # print("Adding point {}".format(round(d*math.cos(angle)), round(width/2 + d*math.sin(angle))))
        if nb_points > 0 :
            sum_x = sum_x/nb_points
            sum_y = sum_y/nb_points
            image[int(sum_y), int(sum_x)] = (255, 0, 0)  # y, x as always
            # Careful, x in image frame is -y lidar frame, and y in image frame is x in lidar frame
            avg_angle = math.atan2(sum_x - width/2, sum_y)
            dist = math.sqrt((sum_x-center_x)**2 + (sum_y-center_y)**2)
            if center_y < sum_y :
                dist *=-1
            dlin_percent = max(-1, min(1, 2*dist/(pixel_per_meter*(range_max - range_min))))
            dang_percent = max(-1, min(1, avg_angle/half_angle))
            print("avg_x_pixels={}, avg_y_pixels){}, avg_angle={}, dist_pixels={}, dlin_percent={}, dang_percent={}, center_y={}".format(sum_x, sum_y, avg_angle, dist, dlin_percent, dang_percent, center_y))
        else :
            dlin_percent = 0
            dang_percent = 0

        if verbose :
            cv2.imshow("image", image)
            # cv2.imwrite("raw" + str(datetime.utcnow())+".png", image)
            cv2.waitKey(1)
        return dlin_percent, dang_percent


    def tick(self):
        try:
            dlin_percent, dang_percent = self.get_barycenter_offset(0.3, 0.7, math.pi/5)
            # return
            target_linear_vel = -BURGER_MAX_LIN_VEL*dlin_percent
            target_angular_vel = BURGER_MAX_ANG_VEL*dang_percent
            twist = Twist()
            twist.linear.x = target_linear_vel
            twist.angular.z = target_angular_vel

            self.pub.publish(twist)
        except:
            rospy.logerr(traceback.format_exc())
            sys.exit()

if __name__ == "__main__":
    try:
        rospy.init_node("simple_follow_me", anonymous=False)

        follow_me = SimpleFollowMe()
        # What to do if shut down (e.g. ctrl + C or failure)
        rospy.on_shutdown(follow_me._shutdown)

        while True:
            follow_me.tick()
            time.sleep(follow_me.tick_period)


        rospy.spin()
    except rospy.ROSInterruptException:
        rospy.logerr(traceback.format_exc())
        follow_me._shutdown()
        sys.exit()

    rospy.loginfo("simple_navigation_goals node terminated.")